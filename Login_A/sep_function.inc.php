<?php
// 03.10.12 WAL
// session start
// if you want to read session items call session_start first
// session_start ();
function var_to_log ($par1, $wa) {
   static $znr =1;
  ob_start();
  var_dump($par1);
  $dat = ob_get_clean();
  $fp = fopen("textfile.txt", $wa);
  fwrite($fp, "$znr: ");
  fwrite($fp, $dat);
  fwrite($fp,  "\r\n");
// be careful of the scope from $znr
  $znr++;
  fclose($fp); 
  return 0;  
}
?>
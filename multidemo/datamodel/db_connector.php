<?php
$host = "";
$user = "";
$pass = "";
$database = "";
// Datenbank-Zugangsdaten, flexibel gesetzt
if ($_SERVER["SERVER_NAME"] == "localhost") {
    $host = "localhost";
    $user = "root";
    $pass = "";
    $database = "db_m133";
} elseif ($_SERVER["SERVER_NAME"] == "haraldmueller.square7.ch") {
    $host = "localhost";
    $user = "haraldmueller";
    $pass = "tbzTBZtbz";
    $database = "haraldmueller";
} elseif ($_SERVER["SERVER_NAME"] == "haraldmueller.bplaced.net") {
    $host = "localhost";
    $user = "haraldmueller_db-m133";
    $pass = "tbzTBZtbz";
    $database = "haraldmueller";
}

/**
 * DB Connection erstellen auf 3 verschiedene Arten zu Schulungszwecken
 * Es ist einem freigestellt, welche der Arten / Stile man auswählt,
 * am Schluss wirken alle gleich
 * @param string $type ("MYSQLI" oder "PDO")
 * @param string $style ("procedural" oder "objektorientiert")
 * @return mysqli|PDO Connection-Variable
 */
function db_open(string $type, string $style) {
    global $host;
    global $user;
    global $pass;
    global $database;
    
    /**
     *     Folgende Aufteilung ist nur zur Schulungszwecken
     *     Eine der 4 Arten reicht. Man kann frei auswählen,
     *     welchen Stil man anwenden will.
     */
    if ($type == "MYSQLI") {
        if ($style == "prozedural") {
            $connection = new mysqli($host, $user, $pass, $database);//Aufbau der Datenbankverbindung
            if ($connection->connect_errno) { //Verbindungspruefung (freiwillig)
                die("Verbindung fehlgeschlagen: " . $connection->connect_error);
            }
            return $connection;
        }
        if ($style == "objektorientiert") {
            $connection = new mysqli($host, $user, $pass, $database);
            if ($connection->connect_error) { //Verbindungspruefung (freiwillig)
                die("Verbindung fehlgeschlagen: " . $connection->connect_error);
            }
            return $connection;
        }
    }
    if ($type == "PDO") { //PDO (=Php Data Object) ist natürlich nur objektorientiert
        try { // versuch Verbindung zu machen und setze den Error-Modus auf Exception
            $connection = new PDO("mysql:host=".$host.";dbname=".$database, $user, $pass);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $connection;
        } catch(PDOException $e) {//fang den Error auf und geb ihn aus
            echo "<br><pre>".$sql . "<br>" . $e->getMessage()."</pre>";
        }
    }
}

function db_close($connection, $type, $style) {
    if ($type == "MYSQLI") {
        if ($style == "procedural") {
            mysqli_close($connection);
        }
        if ($style == "objektorientiert") {
            $connection->close();
        }
    }
    if ($type == "PDO") {
        $connection = null;
    }
}

/**
 * analysiert den SQL auf SELECT und andere 'erste' Wörter,
 * öffnet die Datenbank
 * führt den SQL gegen die Datenbank aus
 * und gibt das ResultSet oder True/False für den Ausführungsstatus zurück
 * @param string $sql - gib hier die SQL-String zur Verarbeitung
 * @param string $type ("MYSQLI" oder "PDO")
 * @param string $style ("procedural" oder "objektorientiert")
 * @return boolean|$resultSet
 */
function dbZugriffAnalyseExecutor(string $sql, string $type, string $style) {
    
    $dbres = null;
    $result = null;
    $resultSet = null;
    
    $sqlStartWith = strtoupper(substr($sql, 0, 6));
    
    if ($sqlStartWith == "SELECT") {
        try {
            if ($type == "PDO") {
                $connection = db_open($type, $style);
                $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $statement = $connection->prepare($sql);
                $statement->execute();
                $dbres = $statement->setFetchMode(PDO::FETCH_ASSOC); 
                foreach ($statement->fetchAll() as $key=>$row) {
                    $resultSet[$row["p_id"]] = $row;
                }
                db_close($connection, $type, $style);
            }
            if ($type == "MYSQLI") {
                if ($style == "objektorientiert") {
                    $connection = db_open($type, $style);
                    $dbres = $connection->query($sql);
                    if ($dbres->num_rows > 0) {
                        while($row = $dbres->fetch_assoc()) {
                            $resultSet[$row["p_id"]] = $row;
                        }
                    }
                    db_close($connection, $type, $style);
                }
                if ($style == "prozedural") {
                    $connection = db_open($type, $style);
                    $dbres = mysqli_query($connection, $sql);
                    if (mysqli_num_rows($dbres) > 0) {
                        //$resultSet = mysqli_fetch_assoc($result);
                        while($row = $dbres->fetch_assoc()) {
                            $resultSet[$row["p_id"]] = $row;
                        }
                    }
                    db_close($connection, $type, $style);
                }
            }
            return $resultSet;
        } catch(PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
            $resultSet = FALSE;
        }
        $result = $resultSet;
        return $result;
    } else { //DROP, "CREATE", "INSERT", "UPDATE", "DELETE"...
        try {
            if ($type == "PDO") {
                $connection = db_open($type, $style);
                $result = $connection->exec($sql); // wenns funktioniert: $result = TRUE;
                db_close($connection, $type, $style);
            }
            if ($type == "MYSQLI") {
                if ($style == "objektorientiert") {
                    $connection = db_open($type, $style);
                    if ($connection->query($sql) === TRUE) {
                        $result = TRUE;
                    }
                    db_close($connection, $type, $style);
                }
                if ($style == "prozedural") {
                    $connection = db_open($type, $style);
                    if (mysqli_query($connection, $sql)) {
                        $result = TRUE;
                    }
                    db_close($connection, $type, $style);
                }
            }
        } catch(PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
            $result = FALSE;
        }
        return $result;
    }
}

?>
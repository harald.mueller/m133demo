<?php if (!isset($_SESSION)) { session_start(); } ?>
<?php include 'view/include.header.html'; ?>

<h1>Userliste</h1>

<?php include 'control/functions_userliste.php'; ?>

<?php 
if (isset($_SESSION["userDetailsAnzeigen"]) AND ($_SESSION["userDetailsAnzeigen"] == TRUE)) {
    unset($_SESSION["userDetailsAnzeigen"]);
    include 'view/userDetailFelder.php';
}
?>

<?= getUserliste(); //hier ist nicht nur die Liste drin, sondern auch alle CRUD-Funktionen  ?>

<?php 
//echo "<pre>REQUEST: ".print_r($_REQUEST, TRUE)."</pre>";// zu Testzwecken
echo "<pre>SESSION: ".print_r($_SESSION, TRUE)."</pre>";// zu Testzwecken
?>

<?php include 'view/include.footer.php'; ?>

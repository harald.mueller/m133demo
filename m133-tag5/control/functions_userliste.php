<?php if (!isset($_SESSION)) { session_start(); }

if (isset($_REQUEST["btn_cancel"])) {
    header("Location: ../userliste.php");
}

if (isset($_REQUEST["addRow"])) {
    //hier müssen Eingabefelder angezeigt werden
    $_SESSION["userDetailsAnzeigen"] = TRUE;
    header("Location: ../userliste.php");
}

if (isset($_REQUEST["btn_addSave"])) {
    $fileName = "../".$_SESSION['fileNameUsers'];
    $userListe = array();
    if (file_exists($fileName)) {
        $json_data = file_get_contents($fileName);
        $userArray = json_decode($json_data, true);
        
        $email = htmlentities($_REQUEST["Email"]); 
        $krzl = htmlentities($_REQUEST["Krzl"]); 
        $name = htmlentities($_REQUEST["Name"]); 
        $ugrp = htmlentities($_REQUEST["UGrp"]); 
        $pass = $_REQUEST["Pass"]; 
        $passwortHash = password_hash($pass, PASSWORD_BCRYPT);

        //Addiert User
        $userArray[$email] = array(
            "Krzl" => $krzl,
            "Name" => $name,
            "UGrp" => $ugrp,
            "Pass" => $passwortHash);
        //Umwandeln und Speichern
        $json_data = json_encode($userArray, JSON_PRETTY_PRINT);
        $isOK = file_put_contents($fileName, $json_data);
        header("Location: ../userliste.php");
    }
        
}
    

if (isset($_REQUEST["editRow"])) {
    echo "EDIT: ".$_REQUEST["editRow"];
    //hier müssen Eingabefelder angezeigt werden
    //am liebsten mit den Werten befüllt, die schon bestehen 
}


if (isset($_REQUEST["deleteRow"])) {
    $delID = $_REQUEST["deleteRow"];
    //echo "DELETE: ".$delID;
    //1.) Userliste lesen (geht in ein Array)
    //2.) entsprechendes Element (aus Array) löschen
    //3.) ganze Userliste spreichern
    $fileName = "../".$_SESSION['fileNameUsers'];
    $userListe = array();
    if (file_exists($fileName)) {
        $json_data = file_get_contents($fileName);
        $userListe = json_decode($json_data, true);
        
        unset($userListe[$delID]);
        
        $json_data = json_encode($userListe, JSON_PRETTY_PRINT);
        $isOK = file_put_contents($fileName, $json_data);  
        
        if ($isOK) {
            header("Location: ../userliste.php");
        } else {
            echo "Error beim löschen aus der User-Datei.";
        }
            
    } else {
        echo "Error: Die User-Datei fehlt!";
    }
}

function getUserliste() {
    $fileName = $_SESSION['fileNameUsers'];  
    if (file_exists($fileName)) {
        $json_data = file_get_contents($fileName);
        $userListe = json_decode($json_data, true);
        //echo "<pre>Test der USERLISTE: ".print_r($userListe, TRUE)."</pre>";
        $out="
            <div id='jumpHere'>
        	  <form action='control/functions_userliste.php' method='post' >
                <table class='table table-striped fa-lg' id='mainTable'>
                    <thead>
                        <tr>
                            <th><button name='addRow' value='addRow' class='button fa fa-plus' /></th>
                            <th>Kürzel</th>
                            <th>Gruppe</th>
                            <th>Name</th>
                        <tr>
                    </thead>
                    <tbody>
                 ";
                    if (count($userListe) < 1) {
                        $out .= "<tr>
                                    <td colspan='4'>Keine Daten vorhanden.<tr>
                                </tr>";
                    } else {
                        foreach ($userListe as $key => $value) {
                            $out .= "<tr>
                                        <td><button name='deleteRow' value='".$key."' class='button-in-liste fa fa-trash'/>
                                            <button name='editRow'   value='".$key."' class='button-in-liste fa fa-edit' /></td>
                                        <td>".$value["Krzl"]."</td>
                                        <td>".$value["UGrp"]."</td>
                                        <td>".$value["Name"]."</td>
                                    </tr>";
                        }
                    }
                    $out .= "
                    </tbody>
                </table>
              </form>
            </div>";
        return $out;
    }
}

?>
-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 02. Apr 2019 um 17:06
-- Server-Version: 10.1.30-MariaDB
-- PHP-Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `db_m133`
--
CREATE DATABASE IF NOT EXISTS `db_m133`;
USE `db_m133`;
-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_arbeitsstunden`
--

CREATE TABLE IF NOT EXISTS `t_arbeitsstunden` (
  `astd_id` int(11) NOT NULL,
  `astd_pers_krzl` varchar(8) NOT NULL,
  `astd_proj_krzl` varchar(8) NOT NULL,
  `astd_datum` date NOT NULL,
  `astd_tarif_krzl` varchar(8) NOT NULL,
  `astd_stdzahl` float NOT NULL,
  `astd_arbeitsbeschreibung` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `t_arbeitsstunden`
--

INSERT INTO `t_arbeitsstunden` (`astd_id`, `astd_pers_krzl`, `astd_proj_krzl`, `astd_datum`, `astd_tarif_krzl`, `astd_stdzahl`, `astd_arbeitsbeschreibung`) VALUES
(1, 'MUH', 'TBZ-IT', '2019-04-22', 'BK', 4, 'Berufskunde-Unterricht M133 Klasse BI16c'),
(2, 'MUH', 'TBZ-IT', '2019-04-23', 'ABU', 3, 'Unterricht ABU Klasse BI16c');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `t_arbeitsstunden`
--
ALTER TABLE `t_arbeitsstunden`
  ADD PRIMARY KEY (`astd_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `t_arbeitsstunden`
--
ALTER TABLE `t_arbeitsstunden`
  MODIFY `astd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.7.7
 */

/**
 * Database `db_m133`
 */

/* `db_m133`.`t_arbeitsstunden` */
$t_arbeitsstunden = array(
  array('astd_id' => '1','astd_pers_krzl' => 'MUH','astd_proj_krzl' => 'TBZ-IT','astd_datum' => '2019-04-22','astd_tarif_krzl' => 'BK','astd_stdzahl' => '4','astd_arbeitsbeschreibung' => 'Berufskunde-Unterricht M133 Klasse BI16c'),
  array('astd_id' => '2','astd_pers_krzl' => 'MUH','astd_proj_krzl' => 'TBZ-IT','astd_datum' => '2019-04-23','astd_tarif_krzl' => 'ABU','astd_stdzahl' => '3','astd_arbeitsbeschreibung' => 'Unterricht ABU Klasse BI16c')
);
